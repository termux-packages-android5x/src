# Termux packages

[![build status](https://api.cirrus-ci.com/github/MadeByThePinsHub/termux-packages-android5x.svg?branch=android-5)](https://cirrus-ci.com/termux/termux-packages)
[![Join the chat at https://gitter.im/termux/termux](https://badges.gitter.im/termux/termux.svg)](https://gitter.im/termux/termux)

**Warning:** android-5 branch is maintained by The Pins Team and the
open-source community, please don't expect any response from us if you having any issues.

This project contains scripts and patches to build packages for the
[Termux](https://termux.com/) Android application. Note that on-device
package building is supported only partially for now.

More information can be found in the project's [Wiki](https://github.com/termux/termux-packages/wiki).

## Repository Links

- Main development repo: <https://gitlab.com/termux-packages-android5x/src>
  - Mostly where the development happens and where merge requests point against.
- Launchpad Mirror: <https://git.launchpad.net/termux-packages-android5x>
  - Backup if GitLab is dooooown.

## Directory Structure

- [disabled-packages](disabled-packages/):
  packages that cannot be built or have serious issues.

- [ndk-patches](ndk-patches/):
  patches for Android NDK headers.

- [packages](packages/):
  all currently available packages.

- [scripts](scripts/):
  utility scripts for building.

## Community

- Official Termux Packages repo for Android 7+: <https://github.com/termux/termux-packages>
- Development Chat on Telegram: <https://t.me/ThePinsTeam_DevOpsChat> (general dev chat) / <https://t.me/joinchat/H6nouPAuCNKo34-c>

