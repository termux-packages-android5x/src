# We'll use Gitpod's official Workspace image for this, and because we have Python and
# Ruby installed, we removed them out.
FROM gitpod/workspace-full

### properties.sh stuff | Ref: https://github.com/termux/termux-packages/blob/master/scripts/properties.sh ####
ENV TERMUX_ANDROID_BUILD_TOOLS_VERSION=30.0.3 \
    # For the Android API level (aka  Android NDK version), consult https://docs.microsoft.com/en-us/xamarin/android/app-fundamentals/android-api-levels?tabs=windows#android-versions-and-api-levels.
    # In our case, we need to focus on Android API level 21 (aka Android 5.x) as our minium API level.
    TERMUX_NDK_VERSION_NUM=21 TERMUX_NDK_REVISION="d" \
    TERMUX_NDK_VERSION=$TERMUX_NDK_VERSION_NUM$TERMUX_NDK_REVISION \
    TERMUX_APP_PACKAGE="com.termux" TERMUX_BASE_DIR="/data/data/$TERMUX_APP_PACKAGE/files" \
    TERMUX_CACHE_DIR="/data/data/$TERMUX_APP_PACKAGE/cache" \
    TERMUX_ANDROID_HOME="$TERMUX_BASE_DIR/home" TERMUX_PREFIX="$TERMUX_BASE_DIR/usr" \
    ## Home directories for SDK and NDK
    ANDROID_HOME="$HOME/.android-sdk" NDK="$HOME/.android-ndk"

### Installing Android SDK and NDK - Step 0 | Ref: https://github.com/termux/termux-packages/blob/master/scripts/setup-android-sdk.sh ###
ENV ANDROID_SDK_FILE=sdk-tools-linux-4333796.zip \
      ANDROID_SDK_SHA256=92ffee5a1d98d856634e8b71132e8a95d96c83a63fde1099be3d86df3106def9 \
      ANDROID_NDK_FILE=android-ndk-r$TERMUX_NDK_VERSION-Linux-x86_64.zip \
      ANDROID_NDK_SHA256=dd6dc090b6e2580206c64bcee499bc16509a5d017c6952dcd2bed9072af67cbd

### Setup dev environment's dependencies | Ref: https://github.com/termux/termux-packages/blob/master/scripts/setup-ubuntu.sh ###
RUN sudo dpkg --add-architecture i386 && sudo apt update -yq \
    && sudo DEBIAN_FRONTEND=noninteractive apt install lzip tar unzip autoconf automake autopoint \
       autogen bison flex g++ g++-multilib gawk gettext gperf intltool \
       libglib2.0-dev libtool-bin libltdl-dev m4 pkg-config scons asciidoc \
       asciidoctor groff help2man pandoc texinfo xmlto xmltoman scdoc ed \
       bsdmainutils valac fig2dev gengetopt swig libdbus-1-dev libexpat1-dev \
       libjpeg-dev lua5.3 libncurses5-dev libc-ares-dev libicu-dev re2c php composer \
       libssl-dev clang-10 libsigsegv-dev zip tcl openssl zlib1g-dev libssl-dev:i386 \
       zlib1g-dev:i386 lld patchelf luajit openjdk-8-jdk libarchive-tools \
       docbook-to-man docbook-utils erlang-nox libgc-dev libgmp-dev libunistring-dev \
       libparse-yapp-perl heimdal-multidev comerr-dev llvm-10-tools llvm-10-dev \
       libevent-dev libreadline-dev libconfig-dev libjansson-dev alex docbook-xsl-ns \
       gnome-common gobject-introspection gtk-3-examples gtk-doc-tools happy itstool \
       libgdk-pixbuf2.0-dev sassc texlive-extra-utils \
       xfce4-dev-tools xfonts-utils xutils-dev libdbus-glib-1-dev-bin sqlite3 \
       protobuf-c-compiler cvs triehash aspell --yes --no-install-recommends

### Installing Android SDK and NDK - Step 1 | Ref: https://github.com/termux/termux-packages/blob/master/scripts/setup-android-sdk.sh ###
RUN mkdir -p {$ANDROID_HOME,$NDK} \
    && echo 'download-hash-debug: $ANDROID_SDK_FILE:$ANDROID_SDK_SHA256 $ANDROID_NDK_FILE:$ANDROID_SDK_SHA256' \
    && curl --fail --retry 3 -o /tmp/android-tools.zip https://dl.google.com/android/repository/$ANDROID_SDK_FILE \
    && curl --fail --retry 3 -o /tmp/android-ndk.zip https://dl.google.com/android/repository/$ANDROID_NDK_FILE \
    && echo "$ANDROID_SDK_SHA256 /tmp/android-tools.zip" | sha256sum -c - \
    && echo "$ANDROID_NDK_SHA256 /tmp/android-ndk.zip" | sha256sum -c - \
    && unzip -q /tmp/android-tools.zip -d $ANDROID_HOME \
    && unzip -q /tmp/android-ndk.zip -d $NDK

### Installing Android SDK and NDK - Step 2 | Ref: https://github.com/termux/termux-packages/blob/master/scripts/setup-android-sdk.sh ###
RUN yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses \
    && yes | $ANDROID_HOME/tools/bin/sdkmanager "platform-tools" "build-tools;$TERMUX_ANDROID_BUILD_TOOLS_VERSION" "platforms;android-28" "platforms;android-24" "platforms;android-21"

### Install Gemfury CLI, among other things (todo: add other deb packaging tools stuff) ###
RUN gem install gemfury \
    && pip3 install docutils recommonmark sphinx

### Cleanup ###
RUN sudo apt-get clean && sudo rm -rfv /var/lib/apt/lists/* \
    && rm -Rfv $NDK/sources/cxx-stl/system \
    && rm -Rfv $ANDROID_HOME/tools/{emulator*,lib*,proguard,templates}
